# GIT  
 * **Git** is a distributed version-control system for tracking the changes in the project source code during the software developments.
 * It is specifically designed for the collobration work among the programmers and professionals on the software developement.
 * It can be used to track the changes during the collobration.
 * And also maintain the chances during the software delevlopment.

## Basic Git commands
 - git init  : It initialize the git on the working area/repository 
 - git add   : It adds the changes from working directory to staging area.
 - git clone : Clone the repository on the local system.
 - git push origin master :saves the files in local repo onto the remote repository.
 - git pull  : fetch the merge chances from the remote repository to your working directory.

**Workspace**         : All the changes are done in this tree of repository.

**Staging**           : All the staged files(ready to be committed) go into this tree of our repository.

**Local Repository**  : All the committed files go to this tree of repository.

**Remote Repository** : This is the copy of our Local Repository stored on server on Internet like (GITHUB and GITLAB).

## GIT Workflow :
* It shows how to manage and work on git by following specific commands:

![GIT Workflow visualizationvisualizations ](https://res.cloudinary.com/practicaldev/image/fetch/s--M_fHUEqA--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://thepracticaldev.s3.amazonaws.com/i/128hsgntnsu9bww0y8sz.png)

### steps to follow :
1. Clone the repository to your local machine:
![Coping the clone repository link](https://github-images.s3.amazonaws.com/enterprise/2.13/assets/images/help/repository/https-url-clone.png)
![git clone command](https://www.techiedelight.com/wp-content/uploads/git-clone-directory.png)

2. Create a new branch:
![branch](https://wac-cdn.atlassian.com/dam/jcr:389059a7-214c-46a3-bc52-7781b4730301/hero.svg?cdnVersion=1266)
* git checkout master

* git checkout -b <branch-name>

3. **Modify** files in your working tree.
* Selectively **stage** just those changes you want to be part of your next commit.

4. GIT add
* git add 

5.  Do a **commit**, to store the files as they are in the staging area permanently to your Local Git Repository.
* git commit -sv
 ![](https://miro.medium.com/max/3092/1*a3rRETgrZ4qw49wALFGzww.png)

6. Do a **push**, to store the new committed changes to the central repository.

* git push origin branch-name











